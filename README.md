# periodic-table-tui

A periodic table for the command line: a fork from [Pryme](https://github.com/pryme-svg/periodic-table-tui), with more data added from [Bowserinator](https://github.com/Bowserinator/Periodic-Table-JSON).

Added :
- Category (noble gas, alkalin metal, transition metal, etc)
- Atomic weight
- Electronic configuration
- Crystal structure
- Units for melting and boiling temperatures, densities
- Radioactivity
- Estimated and disputed results
- Basic movement to lanthanides and actinides (f-group)
- Color legend for electronic configuration group (s, p, d and f)

Remove (from code, but not from the data JSON):
- Specific heat
- Radius
- 1st ionization
- Electrons (same as Protons)

**Tip:** type `quit` or `exit` + `ENTER` to exit without a `C-c`

![Screen](/assets/nuscreen.png?raw=true)
![Demo](/assets/demo.gif?raw=true)
